package com.bitbucket.eventbus.core.interfaces;


import java.util.ArrayList;

public interface Pluggable {
    void publish(Bus bus, String channel, String event, Object data);
    void subscribe(Bus bus, String channel, String event, Object data);
    void stop();
    void start();
    ArrayList<String> getUsedChannels();
}
