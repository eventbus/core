package com.bitbucket.eventbus.core.interfaces;

/**
 * The interface {@code Executable} is implemented in classes
 * that execute subscriber tasks.
 */
public interface Executable {

  /**
   * Execute subscriber task in specified mode.
   * Mode in which task has to be executed is stored inside {@code Task} instance.
   *
   * @param task Subscriber task
   */
  void execute(Runnable task);
}
