package com.bitbucket.eventbus.core.interfaces;

import com.bitbucket.eventbus.core.enums.Mode;

/**
 * The interface {@code Bus} declares main public methods implemented by EventBus.
 * <p>
 * All events are represented by {@code String} literals.
 * To tell event bus how to react on the event, the user must provide event name
 * and the subscriber. Subscribers implement functional interface
 * and can be provided as anonymous inner classes, lambdas or method references. For example:
 * <pre>{@code
 *     on("MyEvent", data -> System.out.println("Event delivered"));
 * }</pre>
 * <p>
 * This statement tells event bus to perform {@code System.out.println("Event delivered")} action
 * whenever event with name {@code "MyEvent"} is sent.
 * <p>
 * To send an event the {@code post()} method must be called. For example:
 * <pre>{@code
 *     post("MyEvent", "MyData");
 * }</pre>
 * <p>
 * As seen above, data can be sent alongside the event. Data is passed as {@code Object}
 * and can be any Java class instance. To be used, data must be properly casted in the subscriber method.
 * In the above example data is instance of {@code String} class, so the example of the subscriber
 * that uses this data can be like this:
 * <pre>{@code
 *     on("MyEvent", data -> {
 *         if (data instanceof String) {
 *           System.out.println((String) data);
 *         }
 *     });
 * }</pre>
 * <p>
 * All events are grouped into channels. If channel name is not specified,
 * events are grouped into default channel.
 * Different channels can have events with the same names.
 * If two channels contain events with the same name, event is delivered only to that channel,
 * on which it is posted. For example:
 * <pre>{@code
 *     on("ChannelOne", "MyEvent", data -> System.out.println("Event delivered to ChannelOne"));
 *     on("ChannelTwo", "MyEvent", data -> System.out.println("Event delivered to ChannelTwo"));
 *     post("ChannelOne", "MyEvent", "MyData");
 * }</pre>
 * <p>
 * Output will contain {@code "Event delivered to ChannelOne"}, because event is sent to the first channel only.
 * <p>
 * Several subscribers can subscribe on the same event on the same channel. For example:
 * <pre>{@code
 *     on("MyChannel", "MyEvent", data -> System.out.println("First subscriber"));
 *     on("MyChannel", "MyEvent", data -> System.out.println("Second subscriber"));
 * }</pre>
 * <p>
 * After {@code post("MyChannel", "MyEvent", "MyData")} is called, output will contain both
 * {@code "First subscriber"} and {@code "Second subscriber"}.
 * <p>
 * To unsubscribe all subscribers from the channel call {@code unsubscribe()} method. For example:
 * <pre>{@code
 *     unsubscribe("MyChannel");
 * }</pre>
 * <p>
 * By default any subscriber is executed on the same thread, from which the corresponding event was posted.
 * Subscribers with background mode specified are executed on background thread. For example:
 * <pre>{@code
 *     on("MyChannel", "MyEvent", data -> System.out.println("Background subscriber"), Task.Mode.BACKGROUND);
 * }</pre>
 * <p>
 * In this case calling {@code post("MyChannel", "MyEvent", "MyData")} does not block current thread.
 */
public interface Bus {

  /**
   * Subscribe to the event on default channel.
   *
   * @param event Name of the event to subscribe to
   * @param task  Subscriber
   * @return Bus instance.
   */
  Bus on(String event, Consumer task);

  /**
   * Subscribe to the event on specified channel.
   *
   * @param channel Channel name
   * @param event   Name of the event to subscribe to
   * @param task    Subscriber
   * @return Bus instance.
   */
  Bus on(String channel, String event, Consumer task);

  /**
   * Subscribe to the event on specified channel in specified mode. Subscriber is executed in specified mode.
   *
   * @param channel Channel name
   * @param event   Name of the event to subscribe to
   * @param task    Subscriber
   * @param mode    Subscriber execution mode
   * @return Bus instance.
   */
  Bus on(String channel, String event, Consumer task, Mode mode);

  /**
   * Subscribe once to the event on default channel.
   * Subscriber is removed from the event bus after the event is posted once.
   *
   * @param event Name of the event to subscribe to
   * @param task  Subscriber
   * @return Bus instance.
   */
  Bus once(String event, Consumer task);

  /**
   * Subscribe once to the event on specified channel.
   * Subscriber is removed from the event bus after the event is posted once.
   *
   * @param channel Channel name
   * @param event   Name of the event to subscribe to
   * @param task    Subscriber
   * @return Bus instance.
   */
  Bus once(String channel, String event, Consumer task);

  /**
   * Subscribe once to the event on specified channel in specified mode. Subscriber is executed in specified mode.
   * Subscriber is removed from the event bus after the event is posted once.
   *
   * @param channel Channel name
   * @param event   Name of the event to subscribe to
   * @param task    Subscriber
   * @param mode    Subscriber execution mode
   * @return Bus instance.
   */
  Bus once(String channel, String event, Consumer task, Mode mode);

  /**
   * Subscribe {@code count} times to the event on default channel. Subscriber is removed from the event bus
   * after the event is posted {@code count} times.
   *
   * @param event Name of the event to subscribe to
   * @param count Number of times the subscriber is executed before removal
   * @param task  Subscriber
   * @return Bus instance.
   */
  Bus many(String event, Integer count, Consumer task);

  /**
   * Subscribe {@code count} times to the event on specified channel. Subscriber is removed from the event bus
   * after the event is posted {@code count} times.
   *
   * @param channel Channel name
   * @param event   Name of the event to subscribe to
   * @param count   Number of times the subscriber is executed before removal
   * @param task    Subscriber
   * @return Bus instance.
   */
  Bus many(String channel, String event, Integer count, Consumer task);

  /**
   * Subscribe {@code count} times to the event on specified channel in specified mode.
   * Subscriber is executed in specified mode.
   * Subscriber is removed from the event bus after the event is posted {@code count} times.
   *
   * @param channel Channel name
   * @param event   Name of the event to subscribe to
   * @param count   Number of times the subscriber is executed before removal
   * @param task    Subscriber
   * @param mode    Subscriber execution mode
   * @return Bus instance.
   */
  Bus many(String channel, String event, Integer count, Consumer task, Mode mode);

  /**
   * Post event on default channel.
   *
   * @param event  Name of the posted event
   * @param object Data posted with the event
   * @return Bus instance.
   */
  Bus post(String event, Object object);

  /**
   * Post event on specified channel
   *
   * @param channel Name of the channel on which event is posted
   * @param event   Name of the posted event
   * @param object  Data posted with the event
   * @return Bus instance.
   */
  Bus post(String channel, String event, Object object);

  /**
   * Unsubscribe all subscribers from specified channel.
   *
   * @param channel Channel name
   * @return Bus instance.
   */
  Bus unsubscribe(String channel);

  Bus withPlugin(Pluggable plugin);
}
