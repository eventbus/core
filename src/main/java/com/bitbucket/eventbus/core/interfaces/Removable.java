package com.bitbucket.eventbus.core.interfaces;

/**
 * The interface {@code Removable} is implemented in classes
 * that remove subscriber tasks from event bus.
 */
public interface Removable {

  /**
   * Remove the subscriber task.
   *
   * @param task Subscriber task to be removed
   */
  void remove(Runnable task);
}
