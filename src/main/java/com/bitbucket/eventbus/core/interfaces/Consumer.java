package com.bitbucket.eventbus.core.interfaces;

/**
 * Represents an operation that accepts a single input argument and returns no
 * result.
 * <p>This is a functional interface</p>
 * whose functional method is {@link #accept(Object)}.
 *
 * @param <T> the type of the input to the operation
 */
public interface Consumer<T> {
  /**
   * Performs this operation on the given argument.
   *
   * @param t the input argument
   */
  void accept(T t);
}
