package com.bitbucket.eventbus.core.enums;

/**
 * Subscriber execution mode.
 */
public enum Mode {
    /**
     * Task is executed on the same thread from which the event was posted.
     */
    CURRENT,
    /**
     * Task is executed on background thread in event bus internal thread pool.
     */
    BACKGROUND
}